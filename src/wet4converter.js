const klaw = require('klaw'),
			path = require('path'),
			fs = require('fs-extra'),
			getFolder = require('./lib/util/getfolder.js'),
			convertFiles = require('./lib/convert-files.js');

/**
 * Main process.<br>
 * Walks through the source folder and any subdirectories and populates an array of paths.<br>
 * Once the entire folder and its subdirectories is read, converts the files and writes them to the output folder.
 *
 * @function
 * @async
 * @returns {Promise.<void>}
 */
module.exports.start = async function() {
	const [sourceFolder, outputFolder] = getFolder();

	let htmlFiles = []; // Is called files, but is actually an array of file paths (including filenames)
	let otherFiles = []; // Same as above

	//recursively walk through all subdirectories ToDo: add copying of non-html files (i.e. images, PDFs, etc.)
	await klaw(sourceFolder, {'fs': fs})
		.on('data', (item) => {
			if (path.extname(item.path) !== '.html') {
				otherFiles.push(path.relative(sourceFolder, item.path)); // these aren't used yet
			} else {
				htmlFiles.push(path.relative(sourceFolder, item.path));
			}
		})
		.on('error', (err, item) => {
			console.error(err);
			console.error('Error occurred in file:' + item);
		})
		.on('end', async () => { // Once all files are read, convert them
			convertFiles(htmlFiles, outputFolder)
				.then(() => {
					console.log('Conversion complete.')
				})
				.catch((e) => {
					console.error('Error: ' + e.stack)
				});
		});
};