const fillTemplate = require('./filltemplate.js'),
      path = require('path'),
      fs = require('fs-extra'),
      getFolder = require('./util/getfolder.js');

/**
 * Takes array of file paths, gets converted data from fillTemplate, and writes all files to output folder.
 *
 * @function
 * @async
 * @param {Array<string>} paths - Array of paths/filenames of files to convert.
 * @param {string} outputDir - Absolute path of output directory.
 * @returns {Promise.<*>} - Resolves when all files are written
 */
module.exports = async function convertFiles(paths, outputDir) {
	outputDir = outputDir || process.env.USERPROFILE + '\\desktop\\WET4-Converted\\';

	// Get all paths for all files in folder specified in command line argument
	let htmlFiles = paths;

	//For each file: get properties and plug into template, returns Promise array
	let files = Promise.all(htmlFiles.map(async file => {
		return await fillTemplate(await fs.readFile(path.join(getFolder()[0], file), 'utf8'));
	}));
	let newFiles = await files; //Need to do this to make sure the Promises complete
	files = null; //Free the memory (I'm not sure it gets garbage collected otherwise)

	// For all files, output to the output directory. Returns Promise array
	return Promise.all(await newFiles.map(async (file, i) => {
		return await fs.outputFile(path.join(outputDir + path.basename(htmlFiles[i])), file, 'utf8');
	})).catch(err => console.log(err));
};