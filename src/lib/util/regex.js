/**
 * Helper function to facilitate getting the first capture group in a regex search.
 * @param regex: The RegExp pattern to be used
 * @param file: String context
 * @returns String: First capture group of the first result found
 */
module.exports = (regex, file) => {
	var result = regex.exec(file);
	if (Array.isArray(result) && result.length < 2 || typeof(result) === 'undefined' || !Array.isArray(result)) {
		return '';
	} else {
		return result[1];
	}
};