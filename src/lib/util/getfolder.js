/**
 * Gets command line arguments for source folder and output folder.
 * @function
 * @return {array} index 0: sourceFolder, index 1: outputFolder
 */
module.exports = exports = function() {
	let sourceFolder;
	let outputFolder;

	try {
		if (process.argv[2]) {
			if (process.argv[2] === '.') {
				sourceFolder = './';
			} else {
				sourceFolder = process.argv[2];
			}
		} else {
			throw new Error('Error: No source directory specified');
		}
		outputFolder = process.argv[3] || process.env.USERPROFILE + '\\desktop\\WET4-Converted\\';
		return [sourceFolder, outputFolder];
	} catch(e) {
		console.error(e);
		process.exit();
	}
};