const document = require('./document.js');

/**
 * Utility functions/maps
 * @namespace
 */

const convertUtils = {
	/**
	 * Basic css classes to replace.<br>
	 * Key: WET2 classes<br>
	 * Value: WET4 classes.
 	 */
	basicDict: {
		'align-right': 'text-right',
		'align-center': 'text-center',
		'align-left': 'text-left',
		//'background-light': '',
		//'background-dark': '',
		'background-accent': 'bg-primary',
		'clear': 'clearfix',
		'equalize': 'wb-eqht',
		'float-right': 'pull-right',
		'float-left': 'pull-left',
		'color-accent': 'text-primary',
		'color-attention': 'text-danger',
		'font-xsmall': 'small',
		'font-small': 'small',
		'font-medium': '',
		'font-large': 'lead',
		'font-xlarge': 'lead',
		'indent-none': 'mrgn-lft-0',
		'indent-small': 'mrgn-lft-sm',
		'indent-medium': 'mrgn-lft-md',
		'indent-large': 'mrgn-lft-lg',
		'indent-xlarge': 'mrgn-lft-xl',
		'margin-bottom-none': 'mrgn-bttm-0',
		'margin-bottom-small': 'mrgn-bttm-sm',
		'margin-bottom-medium': 'mrgn-bttm-md',
		'margin-bottom-large': 'mrgn-bttm-lg',
		'margin-bottom-xlarge': 'mrgn-bttm-xl',
		'margin-top-none': 'mrgn-tp-0',
		'margin-top-small': 'mrgn-tp-sm',
		'margin-top-medium': 'mrgn-tp-sm',
		'margin-top-large': 'mrgn-tp-lg',
		'margin-top-xlarge': 'mrgn-tp-xl',
		'cn-invisible': 'wb-inv',
		'list-bullet-none': 'list-unstyled',
		'list-lower-alpha': 'lst-lwr-alph',
		'list-upper-alpha': 'lst-upr-alph',
		'list-lower-roman': 'lst-lwr-rmn',
		'list-upper-roman': 'lst-upr-rmn',
		'list-numeric': 'lst-num',
		'zebra': 'table-striped',
		'wrap-none': 'nowrap',
		//'table-accent': '',
	},
	/**
	 * 'Module' css classes to replace
	 * Key: WET2 classes
	 * Value: WET4 classes
	 */
	moduleDict: {
		'module-note': 'alert alert-info',
		'module-info': 'alert alert-info',
		'module-alert': 'alert alert-warning',
		'module-attention': 'alert alert-danger'
	},
	/**
	 * Cheerio/jquery selectors to find lvl 2 and lvl 3 &lt;ol&gt;s
	 */
	//could just use css for this, like it was in WET2
	listSelectors: {
		'* li ol li': 'lst-lwr-alpha', //ol lvl 2
		'* li * li ol li': 'lst-lwr-rmn', //ol lvl 3
	},
	/**
	 * span css classes to replace
	 */
	spanDict: {
		'span-1': 'col-md-2',
		'span-2': 'col-md-4',
		'span-3': 'col-md-6',
		'span-4': 'col-md-8',
		'span-5': 'col-md-10',
		'span-6': 'col-md-12',
	},
	/**
	 * Replaces all object keys with their equivalent values (i.e. obj = { 'WET2Class': 'WET4Class' }).
	 * @function
	 * @param {object} dict - find/replace map: (key=find, value=replace)
	 * @param {function} $ - cheerio($) reference
	 * @param {string} selectorPrefix - prefix for cheerio selector (ex. '.' for classes, '#' for IDs, etc.). Optional.
	 */
	replaceAll:
		function(dict, $, selectorPrefix = '') {
			Object.keys(dict).forEach(function (element) {
				$(selectorPrefix + element).each(function() {
					$(this).removeClass(element)
					       .addClass(dict[element]);
				});
			});
		},
	/**
	 * Function for adding list level classes | doesn't work properly right now.
	 * @param {object} dict - Key = cheerio selector, Value = css class
	 * @param {function} $ - cheerio ($) reference
	 */
	addListClasses:
		function(dict, $) {
			Object.keys(dict).forEach(
				function(selector) {
					$(selector).each(function() {
						if ($(this).parent().is('ol') && !$(this).hasClass(dict[selector])) {
							if (selector === '* li ol li' && $(this).parentsUntil().length === 3) {
								$(this).addClass(dict[selector]);
							} else if (selector === '* li * li ol li' && $(this).parentsUntil().length === 5) {
								$(this).addClass(dict[selector]);
							}
						}
					});
				});
	},
	/**
	 * Slices out the 'Note:' part of the note.
	 * @param {RegExp} pattern - Regular expression to find 'Note:'
	 * @param {function} $ - cheerio ($) reference
	 * @param {object} thisVal - reference to 'this'
	 * @returns {boolean} True if pattern is found and sliced.
	 * It's sort of unintuitive and confusing, but I don't feel like changing it.
	 */
	sliceNote:
		function(pattern, $, thisVal) {
			//if pattern is found in the HTML of '$(this)'
			if (pattern.test($(thisVal).html())) {
				let index = pattern.exec($(thisVal).html())[0].length - 1; // return index of last character for slicing
				let slice = $(thisVal).html().slice(index);
				$(thisVal).html(slice.trim());
				return true; //kind of dumb, but just roll with it
			}
    },
};

Object.keys(convertUtils).forEach(key => module.exports[key] = convertUtils[key]); // Exports all properties
