var cheerio = require('cheerio'),
		convertUtils = require('./convertutils.js');

// Shorthand
var basicDict = convertUtils.basicDict,
    modulesDict = convertUtils.moduleDict,
    listDict = convertUtils.listSelectors,
    spanDict = convertUtils.spanDict,
    replaceAll = convertUtils.replaceAll,
		addListClasses = convertUtils.addListClasses,
		sliceNote = convertUtils.sliceNote;

/**
 * Properties are all the convert functions for css classes and components.
 * @function
 * @param {string} content - HTML content to be converted
 * @returns {string} HTML content w/ converted components and css classes
 */
module.exports = function(content) {
	var $ = cheerio.load(content, {decodeEntities: false}); // decodeEntities option is to keep all HTML entities encoded

	//Convert basic css classes
	this.basic = () => {
		replaceAll(basicDict, $, '.');
	};

	//Convert css modules
	this.modules = () => {
		$('.module-note').each(function() { // find potential notes
			var p = $('p');
			var parent = $(this);
			$(this).find(p).each(function() { //iterate through <p> tags
				$(this).find($('strong')).each(function() { // iterate through <strong> tags
					if (/note/i.test($(this).text())) { // find the one with 'note'
						parent.prepend($('<h3>Note</h3>'));
						$(this).remove();
					} else if (/remarque/i.test($(this).text())) { // find the one with 'remarque'
						parent.prepend($('<h3>Remarque</h3>'));
						$(this).remove();
					}
				});
				/* sliceNote returns true if the pattern is found *and* sliced. */
				if (sliceNote(/^\s?note\s*:/i, $, this) === true) {
					parent.prepend($('<h3>Note</h3>'));
				} else if (sliceNote(/^\s?remarque\s*:/i, $, this) === true) {
					parent.prepend($('<h3>Remarque</h3>'));
				}
				sliceNote(/^\s?:\s?/, $, this)
			});
		});
		replaceAll(modulesDict, $, '.'); // replace classes
	};

	// add classes for level 2 and 3 lists (lst-lwr-alph, lst-lwr-rmn)
	this.lists = () => {
		addListClasses(listDict, $);
	};

	// convert tables
	this.tables = () => {
		$('table').each(function() {
			var that = $(this);
			that.addClass('table table-bordered');
			if (that.hasClass('zebra')) {
				that.removeClass('zebra')
						.addClass('table-striped');
			}
			if (that.hasAttribute('width')) {
				that.removeAttr('width')
			}
		})
	};

	this.grid = () => {
		replaceAll(spanDict, $, '.');
		// ToDo:
		// width-n
		// 'border-span-n'
		// others?
	};

	this.basic();
	this.modules();
	this.lists();
	this.tables();
	this.grid();

	return $('body').html();
};