var regex = require('./util/regex.js');
/**
 * Scrapes properties from HTML file.
 * @param {string} file - HTML file contents.
 * @returns {Document} - HTML document model object.
 */

module.exports = function(file) {
	return new Document(file);
};
/**
 * @class
 * @classdesc Document model object.<br> Constructor takes an HTML file in string form
 * @param {string} file - HTML file data
 */
class Document {
	constructor(file) {
		this.title                = regex(/<title>(.+)<\/title>/, file);
		this.metadata             = {};
		this.metadata.title       = regex(/<meta\sname="dcterms\.title"\scontent="(.+)"\s*\/>/, file);
		this.metadata.isHomepage  = regex(/<meta\sname="ManualHomePage"\scontent="(.+)"\s*\/>/, file);
		this.metadata.manualId    = regex(/<meta\sname="ManualID"\scontent="(.+)"\s*\/>/, file);
		if (this.metadata.isHomepage === 'true' || this.metadata.isHomepage === 'True') {
			this.metadata.manualName = this.metadata.title;
		} else {
			this.metadata.manualName  = regex(/<meta\sname="ManualName"\scontent="(.+)"\s*\/>/, file);
		}
		this.metadata.description = regex(/<meta\sname="dc\.description"\scontent="(.+)"\s*\/>/, file);
		if (this.metadata.description === '') {
			this.metadata.description = regex(/<meta\sname="description"\scontent="(.+)"\s*\/>/, file);
		}
		this.metadata.keywords = regex(/<meta\sname="keywords"\scontent="(.+)"\s*\/>/, file);
		this.metadata.creator = regex(/<meta\sname="dc\.creator"\scontent="(.+)"\s*\/>/, file);
		this.metadata.owner = regex(/<meta\sname="owner"\scontent="(.+)"\s*\/>/, file);
		this.metadata.publisher = regex(/<meta\sname="dc\.publisher"\scontent="([\s\S]+?)"\s*?\/>/, file);
		this.metadata.language = regex(/<meta\sname="dc\.language"\stitle="ISO639-2"\scontent="(.+)"\s*\/>/, file);
		this.metadata.issued = regex(/<meta\sname="dcterms\.issued"\stitle="W3CDTF"\scontent="(.+)"\s*\/>/, file);
		this.metadata.modified = regex(/<meta\sname="dcterms\.modified"\stitle="W3CDTF"\scontent="(.+)"\s*\/>/, file);

		this.langFilename =
			regex(/<!--\sInstanceBeginEditable\sname=".*Link.*"\s-->\s*<a\shref\s?=\s?"(.+\.html)"\slang\s?=\s?".+">.+<\/a>/i, file);

		this.breadcrumbs =
			regex(/<!--\sInstanceBeginEditable\sname=".*?Bread\s?crumb.*?"\s-->[\s\S]+?(?:Secure\sManuals|DGCPS|DGSCP)[\s\S]+?<\/li>\s*?(<li.*?><a[\s\S]+?<\/li>)\s*<\/ol>[\s\S]+?<!-- Bread\s?crumb ends/i, file);

		this.tomTitle =
			regex(/<!--\sInstanceBeginEditable\sname=".*Bread\s?crumb.*?"\s-->[\s\S]+?(?:Secure\sManuals|DGCPS|DGSCP).*?<\/a>\s*&#62;\s*<\/li>[\s\S]*?<li.*?><a\shref\s?=\s?".+?">([\s\S]+?)<\/a>/i, file);

		this.tomNumber = regex(/([\d().]{2,7})/, this.tomTitle);

		this.pageTitle =
			regex(/<!--\sInstanceBeginEditable\sname=".*content\stitle.*"\s-->\s*?(.+)\s*?<!--\sInstanceEndEditable\s-->[\s\S]+?<\/h1>/i, file);

		this.toc = regex(/module-table-contents[\s\S]+?(<ul>[\s\S]+?<\/ul>)/i, file);

		this.secMenu =
			regex(/<div\sclass="span-\d\smodule-menu-section.*?">[\s\S]+?<ul>[\s\S]+?(<li[\s\S]+?)\s*?<\/ul>/, file);

		this.nav = {};
		this.nav.prevPage =
			regex(/embedded-nav.+\s+<a\shref\s?=\s?"(.+?)">\s?(?:previous|page\spr&eacute;c&eacute;dente|page\sprécédente)/i, file);
		this.nav.homePage =
			regex(/embedded-nav.+(?:\r\n)?.+(?:\r\n)?\s+<a\shref\s?=\s?"(.+?)">\s?(?:ret)/i, file);
		this.nav.nextPage =
			regex(/embedded-nav.+(?:\r\n)?.+(?:\r\n)?.+(?:\r\n)?\s+<a\shref\s?=\s?"(.+?)">\s?(?:next|Page\s+suivante)/i, file);

		this.content =
			regex(/<!--\sSearchable\scontent\sbegins\s\/\sdebut\sde\sla\srecherche\sdu\scont.+?-->\s+?([\s\S]+)<!--\sSearchable\scontent\sends\s\/\sfin\sde\sla\srecherche\sdu\scontenu\s-->/i, file);
	}
}

