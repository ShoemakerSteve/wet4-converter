/**
 * Install instructions:
 * 1. Download node.js version 7.4 or newer.
 * 2. Add node folder to path if you didn't use the installer.
 * 3. cd into WET4-Converter folder.
 * 4. run 'npm install'.
 * 5. (optional) for testing, run 'npm install --g mocha nyc'.
 */

/**
 * Starts and executes the script.
 * To start, in cmd use:
 * node index.js [sourceFolder] [outputFolder]
 *
 * Basic data flow:
 * 1. wet4converter reads folder and gets all filepaths.
 * 2. convert-files takes paths -> passes to filltemplate
 *    -> filltemplate gets a Document obj (from document.js)
 *       -> uses it to fill a WET4 template and converts components/css classes
 * 3. convert-files takes all the converted files and writes them to the output folder
 */
require('./src/wet4converter.js').start()
	.catch((e) => {
		console.error('Error: ' + e.stack);
	});
