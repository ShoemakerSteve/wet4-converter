const klaw     = require('klaw'),
      path     = require('path'),
      fs       = require('fs-extra'),
      chai     = require('chai'),
      expect   = chai.expect,
			Log = require('./util/log');
chai.config.includeStack = true;

const getFilesContent = require('./util/util').getFilesContent,
      testStrings = require('./string/stringtest'),
			testObjects = require('./object/objecttest');

let testFiles;
const source = '../manuals/TOM2000'; /** Test folder ***************************** **/
let items = [];
let files;
const log = new Log();

var startTests = () =>
	describe('Document properties', function() {
		describe('Objects', () => testObjects(testFiles, log, source));
		describe('Strings', () => testStrings(testFiles, log, source));
	});

// Recursively walk through all subdirectories
klaw(source, { 'fs': fs })
	.on('data', function(item) {
		//console.log('data: ' + item.path);
		if (path.extname(item.path) === '.html') {
			items.push(path.relative(source, item.path));
		}
	})
	.on('error', function(err, item) {
		console.error(err.message);
		console.error('Error occurred reading file:' + item);
	})
	.on('end', function() {
		console.log('Files loaded. Starting tests.');
		files = getFilesContent(items, source); // filesObj
		files.then(files => {
			testFiles = files;
			startTests();
			run(); // mocha function
		}).catch(err => console.error('ERROR: ' + err.stack));
	});

/* Tests */
//function testObjects() {
//	log.addLine('Object errors:');
//	Object.keys(testFiles).forEach(file => {
//		objectTest(file, testFiles[file], log)
//	});
//
//	after('Logging', async function() {
//		await log.writeLog(source, 'object');
//	});
//}
//
//function testStrings() {
//	log.addLine('String errors:');
//	Object.keys(testFiles).forEach(file => {
//		stringTest(file, testFiles[file], log)
//	});
//
//	after('Logging', async function() {
//		await log.writeLog(source, 'string');
//	});
//}
/* /Tests */



// ToDo: Add test for "Date should be dddd-dd-dd", etc.
// ToDo: Add tests for conversion