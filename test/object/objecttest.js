const getDocProps = require('../util/util').getDocProps,
      testFiles = require('../util/util').testFiles,
      chai        = require('chai'),
      expect      = chai.expect;

var checkIfObj = function(val, msg) { expect(val, msg).to.be.a('object') };
var checkIfEmptyObj = function(val, msg) { expect(val, msg).to.not.equal({}) };

module.exports = function(files, log, folder) {
	testFiles('object', files, log, testObjects, folder)
};

function testObjects(filename, data, log) {
	let docProps = getDocProps(data);
	it('Should an object', function(done) {
		log.clearEntry();
		docProps.then(docProps => {
			log.updateEntry(filename, null, null, docProps.docObj);
			Object.keys(docProps.objects).forEach(key => {
				log.updateEntry(null, null, key);
				checkIfObj(docProps.objects[key])
			});
			done();
		}).catch(err => {
			log.updateEntry(null, err);
			log.addToLog();
			done(err);
		});
	});
	it('Should not be empty', function(done) {
		log.clearEntry();
		docProps.then(docProps => {
			log.updateEntry(filename, null, 'docProps.docObj.metadata');
			checkIfEmptyObj(docProps.docObj.metadata);
			if (docProps.docObj.metadata.isHomepage !== 'true') {
				log.updateEntry(null, null, 'docProps.docObj.nav');
				checkIfEmptyObj(docProps.docObj.nav);
			}
			done();
		}).catch(err => {
			log.updateEntry(null, err);
			log.addToLog();
			done(err);
		});
	});
}