const document = require('../../src/lib/document'),
			fs       = require('fs-extra'),
			path     = require('path');

exports.getDocProps = async function(file) {
	var docObj = document(await file);

	var strings = { // Strings that should be strings
		'docObj.title': docObj.title,
		'docObj.metadata.title': docObj.metadata.title,
		'docObj.metadata.isHomepage': docObj.metadata.isHomepage,
		'docObj.metadata.manualId': docObj.metadata.manualId,
		'docObj.metadata.manualName': docObj.metadata.manualName,
		'docObj.metadata.description': docObj.metadata.description,
		'docObj.metadata.keywords': docObj.metadata.keywords,
		'docObj.metadata.creator': docObj.metadata.creator,
		'docObj.metadata.owner': docObj.metadata.owner,
		'docObj.metadata.publisher': docObj.metadata.publisher,
		'docObj.metadata.language': docObj.metadata.language,
		'docObj.metadata.issued': docObj.metadata.issued,
		'docObj.metadata.modified': docObj.metadata.modified,
		'docObj.langFilename': docObj.langFilename,
		'docObj.breadcrumbs': docObj.breadcrumbs,
		'docObj.tomTitle': docObj.tomTitle,
		'docObj.tomNumber': docObj.tomNumber,
		'docObj.pageTitle': docObj.pageTitle,
		'docObj.toc': docObj.toc,
		'docObj.secMenu': docObj.secMenu,
		'docObj.nav.prevPage': docObj.nav.prevPage,
		'docObj.nav.homePage': docObj.nav.homePage,
		'docObj.nav.nextPage': docObj.nav.nextPage,
		'docObj.content': docObj.content,
	};

	var notEmptyStrings = { // Strings that should not be empty
		'docObj.title': docObj.title,
		'docObj.metadata.title': docObj.metadata.title,
		'docObj.metadata.manualId': docObj.metadata.manualId,
		'docObj.metadata.manualName': docObj.metadata.manualName,
		'docObj.metadata.owner': docObj.metadata.owner,
		'docObj.metadata.publisher': docObj.metadata.publisher,
		'docObj.metadata.language': docObj.metadata.language,
		'docObj.metadata.issued': docObj.metadata.issued,
		'docObj.metadata.modified': docObj.metadata.modified,
		'docObj.langFilename': docObj.langFilename,
		'docObj.breadcrumbs': docObj.breadcrumbs,
		'docObj.tomTitle': docObj.tomTitle,
		'docObj.tomNumber': docObj.tomNumber,
		'docObj.pageTitle': docObj.pageTitle,
		'docObj.content': docObj.content,
	};

	var objects = {
		'docObj.metadata': docObj.metadata,
		'docObj.nav': docObj.nav,
	};

	return {
		docObj: docObj,
		strings: strings,
		notEmptyStrings: notEmptyStrings,
		objects: objects
	}
};

exports.getFilesContent = async function(paths, source) { // paths = items
	let filesObj = {}; // key: filename, value: file content

	await Promise.all(paths.map(file => {
		return filesObj[file] = fs.readFile(path.join(source, file), 'utf8');
	}));
	return filesObj;
};

exports.testFiles = function (testName, files, log, testFunc, folder) {
	log.addLine(`${testName} errors:`);
	Object.keys(files).forEach(file => testFunc(file, files[file], log));

	after('Logging', async function() {
		await log.writeLog(folder, testName);
	});
};