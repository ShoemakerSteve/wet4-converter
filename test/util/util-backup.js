const fs = require('fs-extra'),
      path = require('path');

const writePath = './test_logs';

module.exports = exports =
	function writeLog(data, name = 'testlog') {
	//console.log('testlog data:' + data);
	var dateTime = new Date().toLocaleString()
	                         .replace(/ /g, '_')
	                         .replace(/:/g, '_');
		var fileName = `${name}_${dateTime}.txt`;
		var fullPath = path.join(path.resolve(writePath), fileName);
		console.log('Log: ' + fullPath);

		fs.outputFileSync(fullPath, data, 'utf8');
	};
