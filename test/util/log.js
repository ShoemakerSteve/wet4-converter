const fs = require('fs-extra'),
      path = require('path');

/**
 *
 * @type {Log}
 */
module.exports = exports =
	/**
	 * Logs test results to files
	 */
	class Log {
		constructor() {
			this.entries = [];
			this.currentEntry = {
				filename: '',
				error: '',
				stringBeingTested: '',
				props: {}
			};
			this.errorCount = 0;
		}
		updateEntry(filename, error, stringBeingTested, props) {
			if (filename) {
				this.currentEntry.filename = filename;
			}
			if (error) {
				this.currentEntry.error = error;
			}
			if (stringBeingTested) {
				this.currentEntry.stringBeingTested = stringBeingTested;
			}
			if (props) {
				Object.assign(this.currentEntry.props, props)
			}
		}
		clearEntry() {
			this.currentEntry.filename = '';
			this.currentEntry.error = '';
			this.currentEntry.stringBeingTested = '';
			this.currentEntry.props = {};
		}

		/**
		 * Adds entries to log
		 * @param {boolean} docProps
		 * Pass true if you want to add docProps to log, else defaults to true
		 */
		addToLog(docProps = true) {
			let formattedEntry = `Error in ${this.currentEntry.filename}: ${this.currentEntry.error}\nProperty failing test: ${this.currentEntry.stringBeingTested}\n`;

			if (docProps) {
				formattedEntry += JSON.stringify(this.currentEntry.props, null, 2) + '\n';
			}
			this.entries.push(formattedEntry);
			this.errorCount++;
		}

		/**
		 *
		 * @param line
		 */
		addLine(line) {
			this.entries.push(line + '\n');
		}
		writeLog(folder, name) {
			if (this.errorCount === 0) {
				console.log(`No ${name} errors in folder '${folder}'`);
			} else {
				let log = 'Folder: ' + folder + '\nERRORS: ' + this.errorCount + '**********************************\n' + this.entries.join('\n');
				if (name) {
					writeLogFile(log, name)
				} else {
					writeLogFile(log);
				}
			}
		}
	};


function writeLogFile(data, name = 'testlog') {
	const writePath = './test_logs';
	//console.log('testlog data:' + data);
	var dateTime = new Date().toLocaleString()
	                         .replace(/ /g, '_')
	                         .replace(/:/g, '_');
	var fileName = `${name}_${dateTime}.txt`;
	var fullPath = path.join(path.resolve(writePath), fileName);
	console.log('Log: ' + fullPath);

	fs.outputFileSync(fullPath, data, 'utf8');
}