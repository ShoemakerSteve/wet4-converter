const getDocProps = require('../util/util').getDocProps,
      testFiles   = require('../util/util').testFiles,
      chai        = require('chai'),
			expect      = chai.expect;


var checkIfString = function(val, msg) { expect(val, msg).to.be.a('string') };
var checkIfEmptyString = function(val, msg) { expect(val, msg).to.match(/\w+/).and.not.equal('') };

module.exports = function(files, log, folder) {
	testFiles('string', files, log, testStrings, folder)
};

function testStrings(filename, data, log) {
	let docProps = getDocProps(data); // data = testFiles[filename]
	it('Should be a string', function(done) {
		log.clearEntry();
		docProps.then(docProps => {
			log.updateEntry(filename, null, null, docProps.docObj);
			Object.keys(docProps.strings).forEach(key => {
				log.updateEntry(null, null, key);
				checkIfString(docProps.strings[key])
			});
			done();
		}).catch(err => {
			log.updateEntry(null, err);
			log.addToLog();
			done(err);
		});
	});
	it('Should not be empty', function(done) {
		log.clearEntry();
		docProps.then(docProps => {
			log.updateEntry(filename, null, null, docProps.docObj);
			Object.keys(docProps.notEmptyStrings).forEach(key => {
				log.updateEntry(null, null, key);
				checkIfEmptyString(docProps.notEmptyStrings[key]);
			});
			if (!/true/i.test(docProps.docObj.metadata.isHomepage)) {
				log.updateEntry(null, null, 'docProps.docObj.secMenu');
				checkIfEmptyString(docProps.docObj.secMenu);
				log.updateEntry(null, null, 'docProps.docObj.nav.homepage');
				checkIfEmptyString(docProps.docObj.nav.homePage);
			}
			done();
		}).catch(err => {
			log.updateEntry(null, err);
			log.addToLog();
			done(err);
		})
	});
}